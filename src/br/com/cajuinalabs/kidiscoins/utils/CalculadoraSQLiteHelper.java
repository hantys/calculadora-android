package br.com.cajuinalabs.kidiscoins.utils;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class CalculadoraSQLiteHelper extends SQLiteOpenHelper {
	private static final String DATABASE_NAME = "calculadora.db";
	private static final int DATABASE_VERSION = 1;

	private static final String CREATE_USERS_TABLE = "CREATE TABLE [users] ( "
			+ " [id] INTEGER(10),"
			+ " [email] VARCHAR(255)," + " [name] VARCHAR(255));";

	public CalculadoraSQLiteHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		db.execSQL(CREATE_USERS_TABLE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
	}
}
