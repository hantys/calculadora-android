package br.com.cajuinalabs.kidiscoins.utils;

import java.util.List;

import br.com.cajuinalabs.kidiscoins.models.User;
import br.com.cajuinalabs.kidscoins.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ListaUsersAdapter extends ArrayAdapter<User> {
	private ViewHoler holder;

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return super.getCount();
	}

	@Override
	public User getItem(int position) {
		// TODO Auto-generated method stub
		return super.getItem(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return super.getItemId(position);
	}

	private Context context;
    private List<User> users = null;
 
    public ListaUsersAdapter(Context context,  List<User> users) {
        super(context,0, users);
        this.users = users;
        this.context = context;
    }
 
    @Override
    public View getView(int position, View view, ViewGroup parent) {
        User user = users.get(position);
        holder = new ViewHoler();
         
        if(view == null)
            view = LayoutInflater.from(context).inflate(R.layout.item_list_users, null);
 
        ImageView imageViewZombie = (ImageView) view.findViewById(R.id.image_view_user);
        imageViewZombie.setImageResource(R.drawable.images);
         
        TextView textViewNomeZombie = (TextView) view.findViewById(R.id.text_view_nome_user);
        textViewNomeZombie.setText(user.getName());
         
        TextView textViewIdade = (TextView)view.findViewById(R.id.text_view_email_user);
        String textoIdade = String.valueOf(user.getEmail());
        textViewIdade.setText(textoIdade);
 
        return view;
    }
    
    static class ViewHoler {
    	public ImageView image;
    	public TextView name;
    	public TextView email;
    }
}
