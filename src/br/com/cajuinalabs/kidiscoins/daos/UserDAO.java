package br.com.cajuinalabs.kidiscoins.daos;

import java.util.ArrayList;

import br.com.cajuinalabs.kidiscoins.models.User;
import br.com.cajuinalabs.kidiscoins.utils.CalculadoraSQLiteHelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class UserDAO {
	public static final String TABLE = "users";
	public static final String TABLE_ID = "id";
	public static final String TABLE_EMAIL = "email";
	public static final String TABLE_NAME = "name";

	public static final String[] ALL_COLUMNS = { TABLE_ID, TABLE_EMAIL,
			TABLE_NAME };

	private SQLiteDatabase database;
	private CalculadoraSQLiteHelper dbHelper;

	public UserDAO(Context context) {
		dbHelper = new CalculadoraSQLiteHelper(context);
	}

	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
		// Para ativar o uso de chave estrangeira e seus gatilhos
		database.execSQL("PRAGMA foreign_keys=ON;");
	}

	public void close() {
		dbHelper.close();
	}

	public long create(User user) {
		return database.insert(TABLE, null, buildArguments(user));
	}

	public int update(User user) {
		ContentValues values = buildArguments(user);
		return database.update(TABLE, values, TABLE_ID + " = '" + user.getId()
				+ "'", null);
	}

	private ContentValues buildArguments(User user) {
		ContentValues values = new ContentValues();

		values.put(TABLE_ID, user.getId());
		values.put(TABLE_EMAIL, user.getEmail());
		values.put(TABLE_NAME, user.getName());

		return values;
	}

	public void delete(int id) {
		database.delete(TABLE, TABLE_ID + " = '" + id + "'", null);
	}

	public User getDefaultUser() {
		Cursor cursor = database.query(TABLE, ALL_COLUMNS, null, null, null,
				null, null);

		User user = null;
		if (cursor.moveToFirst()) {
			user = cursorToList(cursor);
		}
		cursor.close();
		return user;
	}
	
	public int getCountUser() {
		Cursor cursor = database.rawQuery("SELECT COUNT(*) FROM users;", null);
		cursor.moveToFirst();
		
		if (cursor.getCount() > 0 && cursor.getColumnCount() > 0) {
			return cursor.getInt(0);

		} else {
		    return 0;
		}
	}


	public ArrayList<User> selectAll() {
		ArrayList<User> users = new ArrayList<User>();

		Cursor cursor = database.query(TABLE, ALL_COLUMNS, null, null, null,
				null, null);
		cursor.moveToFirst();

		while (!cursor.isAfterLast()) {
			User user = cursorToList(cursor);
			users.add(user);
			cursor.moveToNext();
		}
		cursor.close();

		return users;
	}

	private User cursorToList(Cursor cursor) {
		User user = new User(0, null, null);

		user.setId(cursor.getInt(0));
		user.setEmail(cursor.getString(1));
		user.setName(cursor.getString(2));

		return user;
	}
}
