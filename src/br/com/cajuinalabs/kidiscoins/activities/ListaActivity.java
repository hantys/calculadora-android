package br.com.cajuinalabs.kidiscoins.activities;

import java.util.List;

import br.com.cajuinalabs.kidiscoins.daos.UserDAO;
import br.com.cajuinalabs.kidiscoins.models.User;
import br.com.cajuinalabs.kidiscoins.utils.ListaUsersAdapter;
import br.com.cajuinalabs.kidscoins.R;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;

public class ListaActivity extends Activity {
	private UserDAO user_dao;
	Button btn_voltar; 
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_lista);
		user_dao = new UserDAO(this);
		user_dao.open();
		btn_voltar = (Button) findViewById(R.id.lista_btn_volta);
		
		ListView listView = (ListView) findViewById(R.id.lista_listview);
        
        ListaUsersAdapter usersAdapter = new ListaUsersAdapter(this, user_dao.selectAll());
        listView.setAdapter(usersAdapter);
        
        btn_voltar.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(ListaActivity.this, TesteActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
			}
		});
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		user_dao.close();
		super.onPause();
	}

}
