package br.com.cajuinalabs.kidiscoins.activities;

import br.com.cajuinalabs.kidiscoins.daos.UserDAO;
import br.com.cajuinalabs.kidiscoins.models.User;
import br.com.cajuinalabs.kidscoins.R;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class TesteActivity extends Activity {
	EditText name, email;
	TextView qnt_users;
	Button btn_enviar;
	private User user;
	private UserDAO user_dao;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_teste);
		user_dao = new UserDAO(this);
		
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		user_dao.open();
		
		user = new User();
		name = (EditText) findViewById(R.id.teste_name);
		email = (EditText) findViewById(R.id.teste_email);
		qnt_users = (TextView) findViewById(R.id.teste_valor_qnt_users);
		btn_enviar = (Button) findViewById(R.id.teste_enviar); 
		qnt_users.setText(String.valueOf(user_dao.getCountUser()));

		
		btn_enviar.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				user.setName(name.getText().toString());
				user.setEmail(email.getText().toString());
				
				user_dao.create(user);
				
				Intent intent = new Intent(TesteActivity.this, ListaActivity.class);
				startActivity(intent);
			}
		});
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		user_dao.close();
	}

}
