package br.com.cajuinalabs.kidiscoins.activities;

import br.com.cajuinalabs.kidscoins.R;
import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class SonActivity extends Activity {
	EditText name;
	Button resultado, limpa, hum, dois, tres, quarto, cinco, seis, sete, oito,
			nove, zero, virgula, soma, sub, div, multi;
	// ArrayList<String> aux = new ArrayList<String>();
	char ultima_string;

	// public Boolean verificaOperacao(ArrayList<String> operacao) {
	// Iterator<String> it = operacao.iterator();
	// while (it.hasNext()) {
	// String s = it.next();
	// if (name.getText().toString().contains(s)) {
	// return false;
	// }
	// }
	// return true;
	// }

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_son);
		name = (EditText) findViewById(R.id.son_name_textfild);
		resultado = (Button) findViewById(R.id.son_btn);
		limpa = (Button) findViewById(R.id.son_ce);
		zero = (Button) findViewById(R.id.son_btn_0);
		hum = (Button) findViewById(R.id.son_btn_1);
		dois = (Button) findViewById(R.id.son_btn_2);
		tres = (Button) findViewById(R.id.son_btn_3);
		quarto = (Button) findViewById(R.id.son_btn_4);
		cinco = (Button) findViewById(R.id.son_btn_5);
		seis = (Button) findViewById(R.id.son_btn_6);
		sete = (Button) findViewById(R.id.son_btn_7);
		oito = (Button) findViewById(R.id.son_btn_8);
		nove = (Button) findViewById(R.id.son_btn_9);
		virgula = (Button) findViewById(R.id.son_btn_virgula);
		soma = (Button) findViewById(R.id.son_btn_mais);
		sub = (Button) findViewById(R.id.son_btn_menos);
		div = (Button) findViewById(R.id.son_btn_dividir);
		multi = (Button) findViewById(R.id.son_btn_muiltiplicacao);
		// aux.add("+");
		// aux.add("-");
		// aux.add("*");
		// aux.add("%");

		limpa.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				name.setText("");
			}
		});

		zero.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				name.setText(name.getText().toString() + zero.getText());
			}
		});

		hum.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				name.setText(name.getText().toString() + hum.getText());
			}
		});

		dois.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				name.setText(name.getText().toString() + dois.getText());
			}
		});

		tres.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				name.setText(name.getText().toString() + tres.getText());
			}
		});

		quarto.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				name.setText(name.getText().toString() + quarto.getText());
			}
		});

		cinco.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				name.setText(name.getText().toString() + cinco.getText());
			}
		});

		seis.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				name.setText(name.getText().toString() + seis.getText());
			}
		});

		sete.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				name.setText(name.getText().toString() + sete.getText());
			}
		});

		oito.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				name.setText(name.getText().toString() + oito.getText());
			}
		});

		nove.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				name.setText(name.getText().toString() + nove.getText());
			}
		});

		virgula.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {

				if (name.getText().toString().compareTo("") == 0) {
					name.setText("0" + virgula.getText());
				} else {
					ultima_string = name.getText().toString()
							.charAt(name.getText().toString().length() - 1);
					if (!(ultima_string == '+' || ultima_string == '-'
							|| ultima_string == '*' || ultima_string == '%' || ultima_string == '.')) {
						name.setText(name.getText().toString()
								+ virgula.getText());
					}
				}

			}
		});

		soma.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				// TODO Auto-generated method stub

				if (!(name.getText().toString().compareTo("") == 0)) {
					ultima_string = name.getText().toString()
							.charAt(name.getText().toString().length() - 1);
					if (ultima_string == '+' || ultima_string == '-'
							|| ultima_string == '*' || ultima_string == '%'
							|| ultima_string == '.') {
						name.setText(name
								.getText()
								.toString()
								.substring(0,
										name.getText().toString().length() - 1)
								+ soma.getText());
					} else {
						name.setText(name.getText().toString() + soma.getText());
					}
				}

			}
		});

		sub.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (!(name.getText().toString().compareTo("") == 0)) {
					ultima_string = name.getText().toString()
							.charAt(name.getText().toString().length() - 1);
					if (ultima_string == '+' || ultima_string == '-'
							|| ultima_string == '*' || ultima_string == '%'
							|| ultima_string == '.') {
						name.setText(name
								.getText()
								.toString()
								.substring(0,
										name.getText().toString().length() - 1)
								+ sub.getText());
					} else {
						name.setText(name.getText().toString() + sub.getText());
					}
				}

			}
		});

		div.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (!(name.getText().toString().compareTo("") == 0)) {
					ultima_string = name.getText().toString()
							.charAt(name.getText().toString().length() - 1);
					if (ultima_string == '+' || ultima_string == '-'
							|| ultima_string == '*' || ultima_string == '%'
							|| ultima_string == '.') {
						name.setText(name
								.getText()
								.toString()
								.substring(0,
										name.getText().toString().length() - 1)
								+ div.getText());
					} else {
						name.setText(name.getText().toString() + div.getText());
					}
				}

			}
		});

		multi.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (!(name.getText().toString().compareTo("") == 0)) {
					ultima_string = name.getText().toString()
							.charAt(name.getText().toString().length() - 1);
					if (ultima_string == '+' || ultima_string == '-'
							|| ultima_string == '*' || ultima_string == '%'
							|| ultima_string == '.') {
						name.setText(name
								.getText()
								.toString()
								.substring(0,
										name.getText().toString().length() - 1)
								+ multi.getText());
					} else {
						name.setText(name.getText().toString()
								+ multi.getText());
					}
				}

			}
		});

		resultado.setOnClickListener(new OnClickListener() {
			@Override
			// Float.parseFloat( meuFloat )
			// Float.toString(25.0f);
			public void onClick(View arg0) {
				Float resultado = (float) 0;
				String aux = "";
				String valor = name.getText().toString();
				char operacao = ' ';

				for (int i = 0; i < valor.length(); i++) {
					if (valor.charAt(i) == '+' || valor.charAt(i) == '-'
							|| valor.charAt(i) == '*' || valor.charAt(i) == '%') {
						if (!(operacao == ' ')) {
							if (operacao == '+') {
								resultado = resultado + Float.parseFloat(aux);
							} else if (operacao == '-') {
								resultado = resultado - Float.parseFloat(aux);
							} else if (operacao == '*') {
								resultado = resultado * Float.parseFloat(aux);
							} else if (operacao == '%') {
								resultado = resultado / Float.parseFloat(aux);
							}

						} else {
							operacao = valor.charAt(i);
							resultado = Float.parseFloat(aux);
							aux = "";
						}
					} else {
						aux = aux + valor.charAt(i);
					}
				}
				if (operacao == '+') {
					resultado = resultado + Float.parseFloat(aux);
				} else if (operacao == '-') {
					resultado = resultado - Float.parseFloat(aux);
				} else if (operacao == '*') {
					resultado = resultado * Float.parseFloat(aux);
				} else if (operacao == '%') {
					resultado = resultado / Float.parseFloat(aux);
				}
				
				if (resultado == 0) {
					name.setText(0);
				}else {
					name.setText(Float.toString(resultado));
				}
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.son, menu);
		return true;
	}

}
